# Just start hello-kafka using docker compose
    > docker compose up --build

The hello-kafka container will exit after submiting and receiving a message to kafka. The kafka container will go on.

# Stop hello-kafka

    > docker compose down


Look at the log file of container hello-kafka.

## Output schould be similar to that:

``202##3-05-11 18:57:27 wait-for-it.sh: waiting 20 seconds for kafka:9092
2023-05-11 18:57:37 wait-for-it.sh: kafka:9092 is available after 10 seconds
2023-05-11 18:57:37 2023/05/11 16:57:37 Topic 'hello-kafka' with 1 partitions successfully created
2023-05-11 18:57:37 time="2023-05-11T16:57:37Z" level=info msg="Send message 'Hello Kafka!' to topic 'hello-kafka'\n"
2023-05-11 18:57:38 time="2023-05-11T16:57:38Z" level=info msg="Successfully send message 'Hello Kafka!' to topic 'hello-kafka'\n"
2023-05-11 18:57:38 time="2023-05-11T16:57:38Z" level=info msg="Start reading message from topic 'hello-kafka'"
2023-05-11 18:57:38 time="2023-05-11T16:57:38Z" level=info msg="Received message from topic 'hello-kafka' at partition 0, offset  0: Hello Kafka!\n"``